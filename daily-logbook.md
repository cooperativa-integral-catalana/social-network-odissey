# 2015: a social network odissey

## The mission a.k.a "leap in the dark"
The old social network used by CIC has become a zombie, several security flaws were reported, the software is not anymore mantained and all the hopes for finding a peaceful, secure and mantained hang-out ecosystem are held by a young group of cibernautes. In this hard mission they will have to find a social ecosystem that meets the following criteria:
* permissions system that support group types.
* possibility of crosspollinization between different groups.
* consensus tools
* federation support or even better distributed networking.

## Daily logbook
All systems prepared...Ignition in 3... 2... 1...
```bash
Retrieving speedtest.net configuration...
Retrieving speedtest.net server list...
Testing from XXXXX XXXXX (xxx.xxx.xxx.xxx)...
Selecting best server based on latency...
Hosted by XXX (xxxxxxxxx) [0.97 km]: 15.744 ms
Testing download speed........................................
Download: 115.04 Mbits/s
Testing upload speed..................................................
Upload: 26.66 Mbits/s
```
All systems up! Preparing to detach...


### UTC 2015-11-10 14:40:37 / day 0 / Anahita

Landing at https://github.com/anahitasocial/anahita

* Languages:
  * 77.7% PHP
  * 21.1% JavaScript
  * 01.2% CSS

* Latest commit:

26 days ago

* Use cases:

1. online learning and knowledge sharing networks
2. information access networks about people, places, and things
3. open science and open data networks
4. online collaboration environments
5. cloud back-end for your mobile apps

* No signs of federation or decentralization.

* Easily customizable.

### UTC 2015-11-10 16:51:30 / day 1 / Bootcamp

* Languages:

  * 42.4% JavaScript
  * 28.2% Python
  * 25.1% HTML
  * 04.3% CSS

* Latest commit:
Aug 25

* Relevant notes:
Uses django web framework, but some wrk should be done in order to apply all criteria.

* No signs of federation or decentralization.

* Easily customizable
